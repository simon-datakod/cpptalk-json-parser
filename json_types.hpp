#pragma once
#include <type_traits>
#include <tuple>
#include "json_helpers.hpp"
#include "json_strings.hpp"

namespace json {
    struct null final { }; 
    template <typename T>
    inline constexpr bool is_null_v = std::is_same_v<T, null>;

    struct result_base {
        static constexpr bool  ok        = true;
        static constexpr bool  has_value = true;
    };

    template <bool Value> 
    struct boolean : public tag::stuff::boolean {
        static constexpr bool value = Value;
    };

    template <int Value> 
    struct integer : public result_base, public tag::stuff::integer {
        static constexpr int value = Value;
    };

    template <typename Str> 
    struct string : public result_base, public tag::stuff::string {
        using value = Str;
        static constexpr std::size_t   length = Str::length;
        static constexpr const char *  c_str  = Str::c_str;
        template <typename OtherStr>
        static constexpr bool equals = meta::str_equals<value, typename OtherStr::value>;
    };

    template <typename Key, typename Value> 
    struct kvpair : public result_base, public tag::stuff::kvpair {
        using key = Key;
        using value = Value;
    };

    template <typename List>
    struct list : public result_base, public tag::stuff::list {
        using items = List;
        static constexpr std::size_t   size = items::size;
        template <std::size_t I>
        using get = typename items :: template get<I>;
    };

    template <typename KvList>
    struct dict : public result_base, public tag::stuff::dict {
        using items = KvList;
        static constexpr std::size_t   size = items::size;
    };

    template <typename ... Items>
    using mk_list = list<meta::list<Items...>>;

    template <typename ... Items>
    using mk_dict = dict<meta::list<Items...>>;
};

