#pragma once
#include <type_traits>
#include "json_helpers.hpp"
#include "json_list.hpp"

namespace json::meta {
    // Compile-time strings
    template <typename C, C... Chars>
    struct str : public tag::meta::str {
        using                        char_type = C;
        static constexpr std::size_t length = sizeof...(Chars);
        static constexpr C           c_str[length + 1] = { Chars..., '\0' };

        template <std::size_t I>
        static constexpr C           get_char = c_str[I];
    };

    namespace detail_str_append {
        template <typename C, typename Str, C X>
        struct append_fu;

        template <typename C, C ... Chars, C X>
        struct append_fu<C, str<C, Chars...>, X> {
            using result = str<C, Chars..., X>;
        };
    }

    template <typename Str, typename Str::char_type X>
    using str_append = typename detail_str_append::append_fu<typename Str::char_type, Str, X>::result;

    namespace detail_str_skip1 {
        template <typename Str>
        struct skip1_Y;

        template <typename C, C First, C...Chars>
        struct skip1_Y<str<C, First, Chars...>> {
            using result = str<C, Chars...>;
        };

        template <typename Str, std::size_t N>
        struct skip1_X {
            using result = typename skip1_Y<Str>::result;
        };

        template <typename Str>
        struct skip1_X<Str, 0> {
            using result = Str;
        };

        template <typename Str>
        struct skip1 {
            using result = typename skip1_X<Str, Str::length>::result;
        };
    }

    template <typename Str>
    using str_skip1 = typename detail_str_skip1::skip1<Str>::result;


    namespace string_literals {
        template <typename C, C... Chars>
        auto operator "" _js() { return str<C, Chars...> { }; }
    }

    template <typename C>
    using empty_string = str<C>;

#   define JSTR(str) decltype(str ##_js)

    namespace detail {
        // `sequence' + helpers from https://blog.galowicz.de/2016/06/24/integer_sequences_at_compile_time/
        template <std::size_t ... Indexes>
        struct seq {};

        template <std::size_t ... Indexes>
        struct gen_seq;

        template <std::size_t I, std::size_t ... Indexes>
        struct gen_seq<I, Indexes...> {
            using type = typename gen_seq<I - 1, I - 1, Indexes...>::type;
        };

        template <std::size_t ... Indexes>
        struct gen_seq<0, Indexes...> {
            using type = seq<Indexes...>;
        };

        template <typename C, typename Str, std::size_t Offs, typename Indexes>
        struct substr1;

        template <typename C, C ... Chars, std::size_t Offs, std::size_t ... Indexes>
        struct substr1<C, str<C, Chars...>, Offs, seq<Indexes...>> {
            using result = str<C, str<C, Chars...> :: template get_char<Offs + Indexes> ...>;
        };

        template <typename Str, std::size_t I, std::size_t N>
        struct substr2 {
            // Wrapper to limit I and N
            static constexpr std::size_t  length = Str::length;
            static constexpr std::size_t  I2     = std::min(I, length);
            static constexpr std::size_t  N2     = std::min(N, length - I2);
            using result = typename substr1<typename Str::char_type, Str, I2, typename gen_seq<N2>::type>::result;
        };
    }

    template <typename Str>
    static constexpr typename Str::char_type str_first = Str:: template get_char<0>;


    template <typename T>
    constexpr std::size_t str_length = T::length;

    template <typename Str, std::size_t I = 0, std::size_t N = str_length<Str> - I>
    using str_substr = typename detail::substr2<Str, I, N>::result;

    template <typename Str1, typename Str2>
    inline constexpr bool str_equals = std::is_same_v<Str1, Str2>;

    namespace detail_str_startswith {
        template <typename Str, typename Prefix>
        struct str_startswith {
            static constexpr bool value = (Str::length >= Prefix::length) &&
                                          str_equals<str_substr<Str, 0, Prefix::length>, Prefix>;
        };
    }

    template <typename Str, typename Prefix>
    static constexpr bool str_startswith = detail_str_startswith::str_startswith<Str, Prefix>::value;

    using namespace string_literals;


    namespace detail {
        template <typename Str1, typename Str2>
        struct str_concat;

        template <typename C, C ... Chars1, C ... Chars2>
        struct str_concat<str<C, Chars1... >, str<C, Chars2...>> {
            using result = str<C, Chars1..., Chars2...>;
        };
    }

    template <typename Str1, typename Str2>
    using str_concat = typename detail::str_concat<Str1, Str2>::result;


    template <typename T>
    static constexpr bool is_string = has_tag<T, tag::meta::str>;


    namespace detail {
        template <typename Separator>
        struct join_with_sep {
            template <typename A, typename B>
            using join = meta::str_concat<meta::str_concat<A, Separator>, B>;
        };
    }

    template <typename StrList, typename Separator = JSTR("")>
    using str_join = meta::list_reduce<detail::join_with_sep<Separator>:: template join, StrList, JSTR("")>;
};




