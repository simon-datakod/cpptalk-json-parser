#pragma once
#include <tuple>
#include <type_traits>
#include "json_helpers.hpp"

namespace json::meta {

    template <typename ... Items>
    struct list : public tag::meta::list {
        using tpl = std::tuple<Items...>;
        static constexpr std::size_t size = sizeof...(Items);

        template <std::size_t I>
        using get = std::tuple_element_t<I, tpl>;
    };


    namespace list_helpers {
        template <typename List1, typename List2>
        struct concat;

        template <typename ... Items1, typename ... Items2>
        struct concat<list<Items1...>, list<Items2...>> {
            using result = list<Items1..., Items2...>;
        };

        template <typename List, typename Item>
        struct append;

        template <typename ... Items, typename Item>
        struct append<list<Items...>, Item> {
            using result = list<Items..., Item>;
        };
    }

    template <typename List1, typename List2>
    using list_concat = typename list_helpers::concat<List1, List2>::result;

    template <typename List, typename Item>
    using list_append = typename list_helpers::append<List, Item>::result;

    template <typename T>
    static constexpr bool is_list = has_tag<T, tag::meta::list>;


    namespace list_helpers_all {
        template <typename List, template<typename> typename Predicate, std::size_t N>
        struct list_all {
            static constexpr bool value = Predicate<typename List::template get<List::size - N>>::value &&
                                          list_all<List, Predicate, N-1>::value;
        };
        template <typename List, template<typename> typename Predicate>
        struct list_all<List, Predicate, 0> {
            static constexpr bool value = true;
        };
    }

    namespace list_helpers_map {
        template <template<typename> typename Func, typename List, typename Accumulated, std::size_t N>
        struct list_map {
            using mapped_value = typename Func<typename List::template get<List::size - N>>::result;
            using list_fu = list_append<Accumulated, mapped_value>;

            using result = typename list_map<Func, List, list_fu, N-1>::result;
        };

        template <template<typename> typename Func, typename List, typename Accumulated>
        struct list_map<Func, List, Accumulated, 0> {
            using result = Accumulated;
        };
    }

    template <typename List, template<typename> typename Predicate>
    static constexpr bool list_all = list_helpers_all::list_all<List, Predicate, List::size>::value;

    template <template<typename> typename Func, typename List>
    using list_map = typename list_helpers_map::list_map<Func, List, list<>, List::size>::result;


    namespace list_reduce_helpers {
        // Result will be `Default' if empty list...
        template <template <typename, typename> typename Func, typename List, typename Default>
        struct reduce {
            template <typename Value, std::size_t N>
            struct reduce_ {
                using result = typename reduce_<Func<Value, typename List:: template get<List::size - N>>, N-1>::result;
            };

            template <typename Value>
            struct reduce_<Value, 0> {
                using result = Value;
            };

            template <typename, std::size_t N>
            struct list_reduce {
                using result = typename reduce_<typename List::template get<0>, N-1  > :: result;
            };

            template <typename _>
            struct list_reduce<_, 0> {
                using result = Default;
            };
            using result = typename list_reduce<void, List::size> :: result;
        };
    }

    template <template <typename, typename> typename Func, typename List, typename Default = void>
    using list_reduce = typename list_reduce_helpers::reduce<Func, List, Default>::result;

    template <typename ListOfLists>
    using list_flatten = list_reduce<list_concat, ListOfLists, list<>>;

    namespace list_filter_helpers {
        template <template<typename> typename Keep, typename List, typename Accumulated, std::size_t N>
        struct filter {
            using                  this_value = typename List:: template get< List::size - N  >;
            static constexpr bool  keep_this  = Keep<this_value> ::value;
            using new_accum = std::conditional_t<keep_this, list_append<Accumulated, this_value>, Accumulated>;
            using result    = typename filter<Keep, List, new_accum, N-1>::result;
        };

        template <template<typename> typename Keep, typename List, typename Accumulated>
        struct filter<Keep, List, Accumulated, 0> {
            using result = Accumulated;
        };
    }

    template <template<typename> typename Keep, typename List>
    using list_filter = typename list_filter_helpers::filter<Keep, List, list<>, List::size>::result;
};

