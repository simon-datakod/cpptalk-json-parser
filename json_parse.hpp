#pragma once
#include <type_traits>
#include <tuple>
#include "json_helpers.hpp"
#include "json_strings.hpp"
#include "json_list.hpp"
#include "json_types.hpp"

namespace json::parse {
    using namespace meta::string_literals;

    namespace result {
        template <typename Tokens>
        struct temporary_match : public tag::parse::result {
            static constexpr bool  ok         = true;
            static constexpr bool  has_value  = false;
            static constexpr bool  has_tokens = true;
            using                  tokens     = Tokens;
        };

        struct value_match : public tag::parse::result {
            static constexpr bool  ok         = true;
            static constexpr bool  has_value  = true;
            static constexpr bool  has_tokens = false;
        };

        struct fail : public tag::parse::result {
            static constexpr bool  ok         = false;
            static constexpr bool  has_value  = false;
            static constexpr bool  has_tokens = false;
        };

        namespace detail {
            template <typename Result>
            struct result_ok_predicate {
                static constexpr bool value = Result::ok;
            };
        }

        template <typename ResultList, typename Rest>
        struct tuple : public tag::parse::tuple {
            using result_list = ResultList;
            using rest        = Rest;
            static constexpr bool ok = meta::list_all<ResultList, detail::result_ok_predicate>;
        };

        template <typename Str, std::size_t N, bool IsOk>
        using make_simple_result_tpl = std::conditional_t<IsOk,
                                           tuple<meta::list<temporary_match<meta::list<meta::str_substr<Str,0,N>>>>, meta::str_substr<Str,N>>,
                                           tuple<meta::list<fail>, Str> >;

        namespace detail {
            template <typename Result>
            struct get_token_list {
                using result = typename Result::tokens;
            };

            template <typename ResultList>
            struct get_all_tokens {
                using result = meta::list_flatten<meta::list_map<get_token_list, ResultList>>;
            };
        }

        template <typename ResultList>
        using all_tokens = typename detail::get_all_tokens<ResultList>::result;

        template <typename ResultList>
        using join_tokens = meta::str_join<all_tokens<ResultList>>;
    }

    namespace ctype {
        static constexpr bool isspace(char c) {
            return c == ' ' || c == '\f' || c == '\n' || c == '\r' || c == '\t' || c == '\v';
        }

        static constexpr bool isdigit(char c) {
            return c >= '0' && c <= '9';
        }

        static constexpr bool is_double_quote(char c) {
            return c == '"';
        }

        static constexpr bool is_single_quote(char c) {
            return c == '\'';
        }

        static constexpr bool is_backslash(char c) {
            return c == '\\';
        }
    }

    namespace detail_parse_literal {
        template <typename Literal, typename Str>
        struct parse_literal {
            static constexpr auto M = meta::str_length<Str>,
                                  N = meta::str_length<Literal>;
            static constexpr bool is_match = M >= N && meta::str_equals<Literal, meta::str_substr<Str,0,N>>;
            using result = result::make_simple_result_tpl<Str,N,is_match>;
        };
    }

    template <typename Literal>
    struct literal {
        template <typename Str>
        using parse = typename detail_parse_literal::parse_literal<Literal, Str>::result;
    };

    namespace detail {
        static constexpr std::size_t count_matches(const char * str, bool (*match_fn)(char)) {
            std::size_t i = 0;
            while (str[i] != '\0' && match_fn(str[i])) {
                i++;
            }
            return i;
        }

        template <bool (*Func)(char), typename Str>
        struct parse_func {
            static constexpr auto N = count_matches(Str::c_str, Func);
            using result = result::make_simple_result_tpl<Str,N,(N>0)>;
        };
    }

    template <bool (*Func)(char)>
    struct from_func {
        template <typename Str>
        using parse = typename detail::parse_func<Func, Str>::result;
    };

    using digits     = from_func<ctype::isdigit>;
    using whitespace = from_func<ctype::isspace>;

    namespace detail {
        template <typename Parser1, typename Parser2>
        struct parse_and {
            template <typename Tuple1, typename Tuple2, bool IsOk>
            struct parse2;

            template <typename Tuple1, typename Tuple2>
            struct parse2<Tuple1, Tuple2, true> {
                using result = result::tuple<meta::list_concat<typename Tuple1::result_list, typename Tuple2::result_list>,
                                             typename Tuple2::rest>;
            };

            template <typename Tuple1, typename Tuple2>
            struct parse2<Tuple1, Tuple2, false> {
                using result = Tuple2;
            };

            template <typename Tuple, bool IsOk>
            struct parse1;

            template <typename Tuple>
            struct parse1<Tuple, false> {
                using result = Tuple;
            };

            template <typename Tuple>
            struct parse1<Tuple, true> {
                using second_tpl = typename Parser2:: template parse<typename Tuple::rest>;
                using result = typename parse2<Tuple, second_tpl, second_tpl::ok>::result;
            };

            template <typename Str>
            struct parse {
                using first_tpl = typename Parser1:: template parse<Str>;
                using result = typename parse1<first_tpl, first_tpl::ok>::result;
            };
        };
    }

    template <typename Parser1, typename Parser2>
    struct And {
        template <typename Str>
        using parse = typename detail::parse_and<Parser1, Parser2>::template parse<Str>::result;
    };

    namespace detail {
        template <typename Parser1, typename Parser2>
        struct parse_or {
            template <typename Tuple1, bool IsOk>
            struct get_fu;

            template <typename Tuple1>
            struct get_fu<Tuple1, true> {
                using result = Tuple1;
            };

            template <typename Tuple1>
            struct get_fu<Tuple1, false> {
                using second_tpl = typename Parser2:: template parse<typename Tuple1::rest>;
                using result = second_tpl;
            };


            template <typename Str>
            struct parse {
                using first_tpl = typename Parser1:: template parse<Str>;
                using result = typename get_fu<first_tpl, first_tpl::ok>::result;
            };
        };
    }

    template <typename Parser1, typename Parser2>
    struct Or {
        template <typename Str>
        using parse = typename detail::parse_or<Parser1, Parser2>:: template parse<Str>::result;
    };


    namespace detail {
        template <typename Parser, typename Str>
        struct parse_optional {
            using tuple = typename Parser:: template parse<Str>;
            using result = std::conditional_t<tuple::ok, tuple, result::make_simple_result_tpl<Str, 0, true>>;
        };
    }

    template <typename Parser>
    struct Optional {
        template <typename Str>
        using parse = typename detail::parse_optional<Parser, Str>::result;
    };

    namespace detail {
        template <typename Parser, typename ResultList, typename Tuple, bool ResultIsMatch>
        struct parse_zero_or_more;

        template <typename Parser, typename ResultList, typename Tuple>
        struct parse_zero_or_more<Parser, ResultList, Tuple, true> {
            using next_tuple = typename Parser:: template parse<typename Tuple:: rest>;
            using result_list = typename parse_zero_or_more<Parser, meta::list_concat<ResultList, typename Tuple::result_list>, next_tuple, next_tuple::ok>::result_list;
        };

        template <typename Parser, typename ResultList, typename Tuple>
        struct parse_zero_or_more<Parser, ResultList, Tuple, false> {
            using result_list = result::tuple<ResultList, typename Tuple::rest>;
        };

        template <typename Parser, typename Str>
        struct zero_or_more {
            using tuple = typename Parser:: template parse<Str>;
            using result = typename parse_zero_or_more<Parser, meta::list<>, tuple, tuple::ok>::result_list;
        };
    }

    template <typename Parser>
    struct ZeroOrMore {
        template <typename Str>
        using parse = typename detail::zero_or_more<Parser, Str>::result;
    };

    namespace detail_parse_string {
        template <typename C>
        struct wrapper {

            template <C Char, typename Enable = void>
            struct map_escape {
                static constexpr C result = Char;
            };

            template <C Char>
            struct map_escape<Char, std::enable_if_t<Char == 'n'>> {
                static constexpr C result = '\n';
            };

            template <C Char>
            struct map_escape<Char, std::enable_if_t<Char == 't'>> {
                static constexpr C result = '\t';
            };

            template <C Char>
            struct map_escape<Char, std::enable_if_t<Char == 'r'>> {
                static constexpr C result = '\r';
            };

            template <typename Str>
            static constexpr C First = Str:: template get_char<0>;

            template <typename Str>
            static constexpr C Second = Str:: template get_char<1>;

            template <typename Str, typename Decoded, typename Enable = void>
            struct X1;

            template <typename Str, typename Decoded>
            struct X1<Str, Decoded, std::enable_if_t<(First<Str> == '"')>> {
                using rest = meta::str_skip1<Str>;
                using match = result::temporary_match<meta::list<Decoded>>;
                using match_list = meta::list<match>;
                using result = result::tuple<match_list, rest>;
            };

            template <typename Str, typename Decoded>
            struct X1<Str, Decoded, std::enable_if_t<(Str::length >= 2) && First<Str> == '\\'>> {
                static constexpr char wtf = map_escape<Second<Str>>::result;
                using result = typename X1<meta::str_skip1<meta::str_skip1<Str>>, meta::str_append<Decoded, wtf>>::result;
            };

            template <typename Str, typename Decoded>
            struct X1<Str, Decoded, std::enable_if_t<(Str::length >= 1) && First<Str> != '"' && First<Str> != '\\'>> {
                using result = typename X1<meta::str_skip1<Str>, meta::str_append<Decoded, First<Str>>>::result;
            };

            template <typename Str, typename Decoded>
            struct X1<Str, Decoded, std::enable_if_t<(Str::length < 1)>> {
                using result = result::tuple<meta::list<result::fail>, Str>;
            };

            template <typename Str, bool HasQuote>
            struct fu1 {
                using result = typename X1<meta::str_skip1<Str>, meta::empty_string<C>>::result;
            };

            template <typename Str>
            struct fu1<Str, false> {
                using result = result::tuple<meta::list<result::fail>, Str>;
            };
        };

        template <typename Str>
        struct parse_str {
            static constexpr bool has_quote = (Str::length > 0) && Str::template get_char<0> == '"';
            using result1 = typename wrapper<typename Str::char_type>::template fu1<Str, has_quote>::result;
            // TODO: FIX THIS! (Hack for wrong `rest' on failure...)
            using result = std::conditional_t<result1::ok, result1,
                                                           result::tuple<meta::list<result::fail>, Str>>;
        };
    }

    struct string_parser {
        template <typename Str>
        using parse = typename detail_parse_string::parse_str<Str>::result;
    };

    namespace extract {
        static constexpr int decimal_str_to_int(const char * str)
        {
            int value = 0, sign = +1;

            if (*str == '+') {
                str++;
            } else if (*str == '-') {
                sign = -1;
                str++;
            }

            while (*str != '\0') {
                value = value * 10 + (*str - '0');
                str++;
            }

            return value * sign;
        }

        static constexpr int hex_digit_value(char ch)
        {
            if (ch >= '0' && ch <= '9') {
                return ch - '0';
            } else if (ch >= 'a' && ch <= 'f') {
                return 10 + (ch - 'a');
            } else if (ch >= 'A' && ch <= 'F') {
                return 10 + (ch - 'A');
            } else {
                return 0;
            }
        }

        static constexpr int hex_str_to_int(const char * str)
        {
            int value = 0;
            if (str[0] == '0' && str[1] == 'x') {
                str += 2;
                while (*str != '\0') {
                    value = value * 16 + hex_digit_value(*str++);
                }
            }
            return value;
        }

        template <bool Value>
        struct bool_result : public result::value_match, public tag::parse::boolean {
            static constexpr bool value = Value;
        };

        template <typename ResultList>
        using decimal_integer = json::integer<decimal_str_to_int(result::join_tokens<ResultList>::c_str)>;

        template <typename ResultList>
        using hex_integer = json::integer<hex_str_to_int(result::join_tokens<ResultList>::c_str)>;

        template <typename ResultList>
        using string = json::string<result::join_tokens<ResultList>>;

        template <typename ResultList>
        using boolean = bool_result<meta::str_equals<result::join_tokens<ResultList>, JSTR("true")>>;

        template <typename Result>
        struct has_value {
            static constexpr bool value = Result::has_value;
        };

        template <typename ResultList>
        using list = json::list<meta::list_filter<has_value, ResultList>>;

        template <typename ResultList>
        using dict = json::dict<meta::list_filter<has_value, ResultList>>;

        template <typename ResultList>
        struct xhelper {
            using filtered = meta::list_filter<has_value, ResultList>;
            using first = typename filtered:: template get<0>;
            using second = typename filtered:: template get<1>;
            using result = json::kvpair<first, second>;
        };


        template <typename ResultList>
        using kvpair = typename xhelper<ResultList>::result;

        template <typename ResultList>
        struct object_helper {
            // For a successful object match, there will only be a single value result
            using result = typename ResultList::template get<0>;
        };

        template <typename ResultList>
        using object = typename object_helper<meta::list_filter<has_value, ResultList>>::result;

        template <template<typename> typename Extractor>
        struct apply_fu {
            template <typename Tuple, bool IsOk>
            struct fu1 {
                using result = result::tuple<meta::list<Extractor<typename Tuple::result_list>>, typename Tuple::rest>;
            };

            template <typename Tuple>
            struct fu1<Tuple, false> {
                using result = Tuple;
            };

            template <typename Tuple>
            struct apply {
                using result = typename fu1<Tuple, Tuple::ok>::result;
            };
        };

        template <template<typename> typename Extractor, typename Parser>
        struct apply {
            template <typename Str>
            using parse = typename apply_fu<Extractor> :: template apply < typename Parser :: template parse<Str> > ::result;
        };
    }

    struct eof {
        template <typename Str>
        struct crap {
            using match1 = result::temporary_match<meta::list<>>;
            using match2 = result::fail;
            using match = meta::list<  std::conditional_t<Str::length == 0, match1, match2>>;
            using result = result::tuple<match, Str>;
        };

        template <typename Str>
        using parse = typename crap<Str>::result;
    };

    // `list' and `dict' are recursive (i.e. a list can contain another list, and so on), and we can't
    // directly refer to `list' from itself, for instance, so we just this dispatch class as a hack
    // to get around that.
    template <typename Tag>
    struct dispatch;

    using integer         = extract::apply<extract::decimal_integer, And<Optional<Or<literal<JSTR("+")>,literal<JSTR("-")>>>, digits>>;
    using string          = extract::apply<extract::string, string_parser>;
    using boolean         = extract::apply<extract::boolean, Or<literal<JSTR("true")>, literal<JSTR("false")>>>;
    using primitive       = Or<boolean, Or<integer, string>>;
    using space           = Optional<whitespace>;

    using value           = Or<Or<primitive, dispatch<tag::parse::list>>, dispatch<tag::parse::dict>>;
    using list_values     = And<And<space, value>, ZeroOrMore< And<And<And<space, literal<JSTR(",")>>, space>, value> >>;
    using list            = extract::apply<extract::list, And<And<literal<JSTR("[")>, Optional<list_values>>, And<space, literal<JSTR("]")>>>>;
    using kvpair          = extract::apply<extract::kvpair, And<string, And<space, And<literal<JSTR(":")>, And<space, value>>>>>;
    using dict_values     = And<And<space, kvpair>, ZeroOrMore< And<And<And<space, literal<JSTR(",")>>, space>, kvpair> >>;
    using dict            = extract::apply<extract::dict, And<And<literal<JSTR("{")>, Optional<dict_values>>, And<space, literal<JSTR("}")>>>>;
    using object          = extract::apply<extract::object, And<And<And<space, value>, space>, eof>>;

    template <>
    struct dispatch<tag::parse::list> {
        template <typename Str>
        using parse = typename list :: template parse<Str>;
    };

    template <>
    struct dispatch<tag::parse::dict> {
        template <typename Str>
        using parse = typename dict :: template parse<Str>;
    };
};

