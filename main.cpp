#include <iostream>
#include "json_strings.hpp"
#include "json_list.hpp"
#include "json_iostream.hpp"
#include "json_parse.hpp"
#include "json_types.hpp"

using namespace json::meta::string_literals;
using namespace json;


using json_data = decltype(
#include "messages.json"
);


using parser = json::parse::object;
using res = parser::parse<json_data>::result_list :: template get<0>;

int main() {
    std::cout << $<res> << "\n";
    return 0;
}
