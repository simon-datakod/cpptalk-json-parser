#pragma once
#include <type_traits>

// TODO: Rename to json_tags.hpp ?

namespace json {
    namespace tag {
        namespace meta {
            struct str { };
            struct list { };
        }

        namespace parse {
            struct result { };
            struct tuple { };

            struct boolean { };
            struct integer { };
            struct string  { };
            struct list    { };
            struct kvpair  { };
            struct dict    { };
        }

        namespace stuff {
            struct boolean { };
            struct integer { };
            struct string  { };
            struct list    { };
            struct kvpair  { };
            struct dict    { };
        }
    }

    template <typename T, typename TTag>
    static constexpr bool has_tag = std::is_base_of_v<TTag, T>;
};

