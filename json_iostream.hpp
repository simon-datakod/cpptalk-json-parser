#pragma once
#include <iostream>
#include "json_helpers.hpp"

namespace json::iostream {

    template <class T>
    typename std::enable_if<json::has_tag<T, tag::parse::result> && T::ok>::type
    write_to_stream(std::ostream& out);

    template <class T>
    typename std::enable_if<json::has_tag<T, tag::parse::result> && !T::ok>::type 
    write_to_stream(std::ostream& out);


    template <class T>
    typename std::enable_if<json::has_tag<T, tag::stuff::boolean>>::type 
    write_to_stream(std::ostream& out, std::size_t max_len = 0);

    template <class T>
    typename std::enable_if<json::has_tag<T, tag::stuff::integer>>::type 
    write_to_stream(std::ostream& out, std::size_t max_len = 0);

    template <class T>
    typename std::enable_if<json::has_tag<T, tag::stuff::string>>::type 
    write_to_stream(std::ostream& out, std::size_t max_len = 0);

    template <class T>
    typename std::enable_if<json::has_tag<T, tag::stuff::kvpair>>::type 
    write_to_stream(std::ostream& out, std::size_t max_len = 0);

    template <class T>
    typename std::enable_if<json::has_tag<T, tag::stuff::list>>::type 
    write_to_stream(std::ostream& out, std::size_t max_len = 0);

    template <class T>
    typename std::enable_if<json::has_tag<T, tag::stuff::dict>>::type 
    write_to_stream(std::ostream& out, std::size_t max_len = 0);


    namespace detail {



        static void write_quoted_string(std::ostream& out, const char * str, std::size_t max_len = 0) {
            out << '"';
            std::size_t i = 0;
            for (;;) {
                char ch = str[i];
                if (ch == '\0') {
                    break;
                } else if (max_len > 0 && i+1 > max_len) {
                    out << "..";
                    break;
                } else if (ch == '\n') {
                    out << "\\n";
                } else if (ch == '\t') {
                    out << "\\t";
                } else if (ch == '\r') {
                    out << "\\r";
                } else if (ch == '"') {
                    out << '\\' << ch;
                } else {
                    out << ch;
                }
                i++;
            }
            out << '"';
        }
    }

    //==  meta::str  =======================================
    template <class T>
    typename std::enable_if<json::has_tag<T, tag::meta::str>>::type 
    write_to_stream(std::ostream& out, std::size_t max_len = 0) {
        detail::write_quoted_string(out, T::c_str, max_len);
    }


    //==  meta::list  ======================================
    template <class T>
    typename std::enable_if<json::has_tag<T, tag::meta::list>>::type 
    write_to_stream(std::ostream& out);

    namespace list_helpers {
        template <class T, std::size_t N, std::size_t I>
        struct list_writer {
            static constexpr std::size_t J = N - I;
            static void write_to_stream(std::ostream& out) {
                iostream::write_to_stream<typename T:: template get<J>>(out);
                if (I > 1) {
                    out << ", ";
                    list_writer<T, N, I-1>::write_to_stream(out);
                }
            }
        };

        template <class T, std::size_t N>
        struct list_writer<T, N, 0> {
            static void write_to_stream(std::ostream& out) {
            }
        };
    }

    template <class T>
    typename std::enable_if<json::has_tag<T, tag::meta::list>>::type 
    write_to_stream(std::ostream& out) {
        out << "[";
        list_helpers::list_writer<T, T::size, T::size>::write_to_stream(out);
        out << "]";
    }

    //==  parse::result  ===================================
    template <class T>
    typename std::enable_if<json::has_tag<T, tag::parse::result> && T::ok>::type 
    write_to_stream(std::ostream& out) {
        out << "result_match<" ;
        if constexpr (T::has_tokens) {
            write_to_stream<typename T::tokens>(out);
        } else if constexpr (T::has_value) {
            if constexpr (json::has_tag<T, tag::parse::string>) {
                out << "STR: ";
                out << T::value::c_str;
            } else if constexpr (json::has_tag<T, tag::parse::list>) {
                out << "LIST:";
                write_to_stream<typename T::value>(out);
            } else if constexpr (json::has_tag<T, tag::parse::kvpair>) {
                out << "KVPAIR:";
                write_to_stream<typename T::value>(out);
            } else if constexpr (json::has_tag<T, tag::parse::dict>) {
                out << "DICT:";
                write_to_stream<typename T::value>(out);
            } else {
                out << T::value;
            }
        }

        // out << ", rest=";
        // write_to_stream<typename T::rest>(out, 10);
        out << ">";
        /*out << " [tags:";
        if (has_tag<T, tag::extract::value>)   { out << " value"; }
        if (has_tag<T, tag::extract::integer>) { out << " integer"; }
        if (has_tag<T, tag::extract::boolean>) { out << " boolean"; }
        if (has_tag<T, tag::extract::string>)  { out << " string"; }
        out << "]"; */
    }


    template <class T>
    typename std::enable_if<json::has_tag<T, tag::parse::result> && !T::ok>::type 
    write_to_stream(std::ostream& out) {
        out << "result_fail";
    }

    //==  parse::tuple  ====================================
    template <class T>
    typename std::enable_if<json::has_tag<T, tag::parse::tuple>>::type 
    write_to_stream(std::ostream& out) {
        out << "tuple<";
        write_to_stream<typename T::result_list>(out);
        out << ", ";
        write_to_stream<typename T::rest>(out);
        out << ">";
    }

    //==  json::boolean  ===================================
    template <class T>
    typename std::enable_if<json::has_tag<T, tag::stuff::boolean>>::type 
    write_to_stream(std::ostream& out, std::size_t max_len) {
        out << (T::value ? "true" : "false");
    }

    //==  json::integer  ===================================
    template <class T>
    typename std::enable_if<json::has_tag<T, tag::stuff::integer>>::type 
    write_to_stream(std::ostream& out, std::size_t max_len) {
        out << T::value;
    }

    //==  json::string  ====================================
    template <class T>
    typename std::enable_if<json::has_tag<T, tag::stuff::string>>::type 
    write_to_stream(std::ostream& out, std::size_t max_len) {
        detail::write_quoted_string(out, T::value::c_str, max_len);
    }

    //==  json::kvpair  ====================================
    template <class T>
    typename std::enable_if<json::has_tag<T, tag::stuff::kvpair>>::type 
    write_to_stream(std::ostream& out, std::size_t max_len) {
        write_to_stream<typename T::key>(out, max_len);
        out << ": ";
        write_to_stream<typename T::value>(out, max_len);
    }

    struct xhelper {
        template <typename List, std::size_t N>
        struct write_stuff {
            static void write_to_stream(std::ostream& out) {
                using item = typename List :: template get<List::size - N>;
                json::iostream::write_to_stream<item>(out);
                if constexpr (N > 1) {
                    out << ", ";
                    write_stuff<List,N-1>::write_to_stream(out);
                }
            }
        };

        template <typename List>
        struct write_stuff<List, 0> {
            static void write_to_stream(std::ostream& out) {
            }
        };
    };


    //==  json::list  ======================================
    template <class T>
    typename std::enable_if<json::has_tag<T, tag::stuff::list>>::type 
    write_to_stream(std::ostream& out, std::size_t max_len) {
        out << "[ ";
        using items = typename T::items;
        xhelper::write_stuff<items, items::size>::write_to_stream(out);
        out << " ]";
    }

    //==  json::dict  ======================================
    template <class T>
    typename std::enable_if<json::has_tag<T, tag::stuff::dict>>::type 
    write_to_stream(std::ostream& out, std::size_t max_len) {
        out << "{ ";
        using items = typename T::items;
        xhelper::write_stuff<items, items::size>::write_to_stream(out);
        out << " }";
    }
};


namespace json {
    namespace detail {
        template <typename T>
        struct stream_wrapper {
            inline void write_to_stream(std::ostream& out) const {
                json::iostream::write_to_stream<T>(out);
            }
        };

        template <typename T>
        inline std::ostream& operator<<(std::ostream& out, const stream_wrapper<T>& wrapper) {
            wrapper.write_to_stream(out);
            return out;
        }
    }

    template <typename T>
    static constexpr auto $ = detail::stream_wrapper<T>();
}
